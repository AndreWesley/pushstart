﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Authenticator : MonoBehaviour {

	#region fields
	[SerializeField] private InputField userField;
	[SerializeField] private InputField passwordField;
	[SerializeField] private Toggle rememberFields;
	[SerializeField] private Text errorMessage;
	[SerializeField] private GameObject loadingConnection;
	#endregion

	#region UnityMessages
	void Start () {
		errorMessage.text = "";
		if (PlayerPrefs.GetInt ("RememberFields") == 1) {
			userField.text = PlayerPrefs.GetString ("user");
			passwordField.text = PlayerPrefs.GetString ("password");
			rememberFields.isOn = true;
		} else {
			rememberFields.isOn = false;
		}
	}

	void OnDestroy() {
		PlayerPrefs.SetString ("user", rememberFields.isOn ? userField.text : "");
		PlayerPrefs.SetString ("password", rememberFields.isOn ? passwordField.text : "");
		PlayerPrefs.SetInt ("RememberFields", rememberFields.isOn ? 1 : 0);
	}
	#endregion

	#region connection area
	public void Login() {
		if (!String.IsNullOrEmpty (userField.text) && !String.IsNullOrEmpty (passwordField.text)) {

			//encrypt the password using SHA256 hashkey, pass it together with username to json and request the authentication to server
			User user = new User (userField.text, Security.ConvertToSHA256 (passwordField.text));
			string userInfo = JsonUtility.ToJson (user, true);
			ServerConnector.GetInstance.AuthenticationRequest (userInfo);
			StartCoroutine (ConnectionStatus ());
		} else {
			ServerConnector.GetInstance.UsernameOrPasswordAreEmpty();
			StartCoroutine (ConnectionStatus ());
		}
	}

	private IEnumerator ConnectionStatus() {
		loadingConnection.SetActive (true);
		errorMessage.enabled = false;
		//verify the server status and if is in login process; if it is, let the connecting animation running until connect
		bool status = ServerConnector.GetInstance.status == ServerStatus.LOGGING_IN || ServerConnector.GetInstance.status == ServerStatus.GETTING_AUTHORIZATION || ServerConnector.GetInstance.status == ServerStatus.CONNECTED;
		while (status) {
			status = ServerConnector.GetInstance.status == ServerStatus.LOGGING_IN || ServerConnector.GetInstance.status == ServerStatus.GETTING_AUTHORIZATION || ServerConnector.GetInstance.status == ServerStatus.CONNECTED;
			yield return new WaitForFixedUpdate ();
			if (ServerConnector.GetInstance.status == ServerStatus.CONNECTED) {
				yield break;
			}
		}
		//if the status is error, then disable the connection animation and show the error message
		loadingConnection.SetActive (false);
		errorMessage.enabled = true;
		errorMessage.text = ServerConnector.GetInstance.errorMessage;
	}
	#endregion
}