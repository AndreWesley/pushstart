﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this class is just a to drag n drop the objct on OnClick (need to be a public non-static method,
//and the main class (audio manager) jumps to scenes and i cant drag n drop it
public class Click : MonoBehaviour {
	public void ClickFX() {
		AudioManager.GetInstance.Play (SoundFX.CLICK);
	}
}