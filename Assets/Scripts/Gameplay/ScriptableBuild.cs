﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuildType {
	HOUSES,
	FACTORY,
	MALL,
	PARK,
	FARM
}

[CreateAssetMenu]
public class ScriptableBuild : ScriptableObject {

	public BuildType type;
	public Sprite sprite;
	public float buildTime;
	public int price;

	public float periodicity;

	public int income;
	public int food;
	public int lifeQuality;
	public int people;

	public int startIncome;
	public int startFood;
	public int startLifeQuality;
	public int startPeople;
}