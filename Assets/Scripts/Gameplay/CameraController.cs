﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	#region fields
	[SerializeField] Transform horizontalLimit;
	[SerializeField] Transform verticalLimit;
	[SerializeField] private float distance = 15f;
	[SerializeField] private LayerMask layerMask;
	[Range(0f,1f)] [SerializeField] private float speed = 1f;

	private bool zooming;
	private Vector3 startInput;
	private Vector3 lostInput;
	private float startDistance;
	private float startZoom;

	public bool canUseZoom;
	public bool moving;

	#endregion //fields

	#region unity messages
	void Start () {
		canUseZoom = true;
		startZoom = Camera.main.orthographicSize;
		zooming = false;
		moving = false;
		startDistance = 0f;
	}
	
	void Update () {
		MoveCamera ();
		Zoom ();
	}//end of update

	#endregion //unity messages

	#region my methods
	//clamp the camera position to stay inside of game's limits
	private void ClampCameraPosition() {
		Vector3 cameraClampedPosition = transform.position;
		cameraClampedPosition.x = Mathf.Clamp (cameraClampedPosition.x, -horizontalLimit.position.x, horizontalLimit.position.x);
		cameraClampedPosition.y = Mathf.Clamp (cameraClampedPosition.y, -verticalLimit.position.y, verticalLimit.position.y);
		transform.position = cameraClampedPosition;
	}

	//make the zoom in/out based on distance of the fingers during the multi touch
	private void Zoom() {
		if (Input.touchCount > 1 && canUseZoom) {
			if (!zooming) {
				zooming = true;
				startDistance = Vector2.Distance (Input.GetTouch (0).position, Input.GetTouch (1).position);
				startZoom = Camera.main.orthographicSize;
			}

			float currentDistance = Vector2.Distance (Input.GetTouch (0).position, Input.GetTouch (1).position);
			Camera.main.orthographicSize = Mathf.Clamp(startDistance * startZoom / currentDistance,3f,7f);

		} else {
			startDistance = 0f;
			zooming = false;
		}
	}

	//move camera based on finger swipe
	private void MoveCamera() {
		if (Input.touchCount > 0) {
			Vector2 touchCoord = Input.GetTouch (0).position;
			Vector3 touchPosition = Camera.main.ScreenToWorldPoint (new Vector3 (touchCoord.x, touchCoord.y));
			RaycastHit2D hit = Physics2D.Raycast(touchPosition,Vector2.zero,distance);
			if (hit.collider != null && !GameManager.GetInstance.isDraggingBuilding) {
				transform.position += new Vector3 (Input.GetTouch (0).deltaPosition.x,Input.GetTouch (0).deltaPosition.y,0f) * Time.deltaTime * -speed;
				ClampCameraPosition ();
			}
		}
	}//end of move camera method

	#endregion //my methods

}//end of CameraController Class
