﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	#region fields
	private static GameManager instance;
	private int money;
	private int food;
	private int lifeQuality;
	private int people;
	public PoolManager buildingPool;
	public ResourcesManager coinsManager;

	[SerializeField] Text moneyText;
	[SerializeField] Text nickNameText;
	[SerializeField] Text foodText;
	[SerializeField] Text lifeQualityText;
	[SerializeField] Text peopleText;
//	[SerializeField] private AudioManager audioManager;

	public ScriptableBuild park, houses, farm, factory, mall;
	public Transform CoinIcon, FoodIcon, PeopleIcon, LifeQualityIcon;
	public List<BuildSelectButton> buildButtons;
	public bool isDraggingBuilding;
	public MySceneManager sceneManager;
	public CameraController camController;

	#endregion //fields

	#region unity messages
	void Awake() {
		food = 0;
		lifeQuality = 0;
		people = 0;
	}

	void Start() {
//		audioManager = ServerConnector.GetInstance.audioManager;
		isDraggingBuilding = false;
		money = ServerConnector.GetInstance.loginInfo.profile.money;
		moneyText.text = money.ToString ();
		foodText.text = food.ToString ();
		lifeQualityText.text = lifeQuality.ToString ();
		peopleText.text = people.ToString ();
		nickNameText.text = ServerConnector.GetInstance.loginInfo.profile.nickname;
		for (int i = 0; i < buildButtons.Count; i++) {
			buildButtons [i].CheckEconomy ();
		}
	}

	#endregion //unity messages

	#region singleton inspired structure
	//inspired on singleton pattern
	public static GameManager GetInstance {
		get { return instance != null ? instance : instance = FindObjectOfType<GameManager> (); }
	}
	#endregion

	#region my methods

	public int Money {
		get { return money; }

		//inspired on observer pattern... this will verify and warn up the buildbuttons
		//to check if the buildings can be buyed or not
		set {
			money = value;
			for (int i = 0; i < buildButtons.Count; i++) {
				buildButtons [i].CheckEconomy ();
			}//end of for
			moneyText.text = money.ToString ();
		}//end of set
	}//end of get-set money

	public void Logout() {
		AudioListener.pause = false;
		ServerConnector.GetInstance.LogoutRequest();
	}

	public int Food {
		get { return food; }
		set {
			food = value;
			foodText.text = food.ToString ();
			if (food < 0) {
				GameOver ();
			}
		}
	}//end of get-set

	public int LifeQuality {
		get { return lifeQuality; }
		set {
			lifeQuality = value;
			lifeQualityText.text = lifeQuality.ToString ();
			if (lifeQuality < 0) {
				GameOver ();
			}
		}
	}//end of get-set

	public int People {
		get { return food; }
		set { people = value;
			peopleText.text = people.ToString ();
		}
	}//end of get-set
		
	private void GameOver() {
		if (people > PlayerPrefs.GetInt ("HighScore")) {
			PlayerPrefs.SetInt ("HighScore", people);
			PlayerPrefs.Save ();
			PlayerPrefs.SetInt ("NewRecord", 1);
		}
		PlayerPrefs.SetInt ("YourScore", people);
		GameManager.GetInstance.sceneManager.GameOver ();
	}

	#endregion //my methods
}//end of class
