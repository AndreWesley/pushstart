﻿public class User {
	public string username;
	public string password;

	public User (string username, string password) {
		this.username = username;
		this.password = password;
	}

}

[System.Serializable]
public class LoginInfo {
	public Profile profile;
	public string token;
	public long expires;
}

[System.Serializable]
public class Profile {
	public string name;
	public string type;
	public string nickname;
	public int money;
}