﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;


public enum SoundTrack {
	INTRO,
	CITY
}

public enum SoundFX {
	CLICK,
	COIN
}

public class AudioManager : MonoBehaviour {

	[SerializeField] private AudioClip cityST;
	[SerializeField] private AudioClip introST;

	[SerializeField] private AudioSource source;
	[SerializeField] private AudioMixerGroup mixerGroup;
	[SerializeField] private AudioClip ClickSFX;
	[SerializeField] private AudioClip CoinSFX;

	private static AudioManager instance;

	public static AudioManager GetInstance {
		get { return instance != null ? instance : instance = FindObjectOfType<AudioManager> (); }
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.A))
		source.Play ();
	}

	void Awake() {
		SceneManager.sceneLoaded += OnLoadedScene;
	}

	public void Play (SoundFX sfx) {
		switch (sfx) {
		case SoundFX.CLICK:
			source.PlayOneShot (ClickSFX);
			break;
		case SoundFX.COIN:
			source.PlayOneShot (CoinSFX);
			break;
		}

	}

	public void Stop() {
		StartCoroutine (FadingOut ());
	}

	public void Play (SoundTrack st) {
		AudioClip clip = null;
		switch (st) {
		case SoundTrack.INTRO:
			clip = introST;
			break;
		case SoundTrack.CITY:
			clip = cityST;
			break;
		}
		source.clip = clip;
		source.Play ();
		StartCoroutine (FadingIn ());
	}

	//fade in/out the audio listener
	private IEnumerator FadingOut() {

		AudioListener.volume = 1f;
		while (AudioListener.volume > 0.05f) {
			AudioListener.volume -= Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}
		AudioListener.volume = 0f;
	}//end of fading out

	private IEnumerator FadingIn() {
		AudioListener.volume = 0f;
		source.Play ();
		while (AudioListener.volume < 0.95f) {
			AudioListener.volume += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}
		AudioListener.volume = 1f;
	}//end of fading in

	//method that will be delegate to run every time that this object jump to another scene
	public void OnLoadedScene(Scene scene, LoadSceneMode mode)  {
		if (ServerConnector.GetInstance.audioManager == null) {
			ServerConnector.GetInstance.audioManager = this;
		}

		//set the right soundtrack looking to new scene
		if (ServerConnector.GetInstance.audioManager.Equals (this)) {
			switch (scene.name) {
			case "Gameplay":
				Play (SoundTrack.CITY);
				break;
			default: 
				if (!source.clip.Equals (introST))
					Play (SoundTrack.INTRO);
				break;
			}

		}//end if audio == this
	}//end of on loaded scene method

	//this mixer set a lowpass effect and a lower pitch to make the soundtrack more "heavy"
	//is used on game over and when get out of that scene, this method is called again
	//to take out the mixer
	public void SetMixer(bool active) {
		source.outputAudioMixerGroup = active ? mixerGroup : null;
	}
}//end of class