﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CollectableType {
	COIN,
	FOOD,
	PEOPLE,
	LIFE_QUALITY
}

public enum CoinType {
	NONE,
	COIN,
	SUPER_COIN,
	MEGA_COIN
}

[RequireComponent(typeof(Rigidbody2D),typeof(SpriteRenderer),typeof(Animator))]
public class Collectable : MonoBehaviour {

	#region fields
	[Range(0f,1f)] [SerializeField] private float smooth;
	[Range(1f,10f)] [SerializeField] private float rangeForce;
	[SerializeField] private ParticleSystem particles;
	[SerializeField] private float speed;
	[SerializeField] private int value;
	[SerializeField] private CollectableType type;
	[SerializeField] private CoinType coinType;

	private Rigidbody2D rb;
	private SpriteRenderer spriteR;
	private Animator animator;
	private bool hasBlasted;
	private Vector2 velocity;
	private float distance;
	private bool beingCollected;
	private Transform target;
	private Color myColor;
	private bool spawnedNow;
	#endregion //fields

	#region unity messages
	void Awake() {
		spawnedNow = true;
		rb = GetComponent<Rigidbody2D>();
		spriteR = GetComponent<SpriteRenderer> ();
		animator = GetComponent <Animator> ();
		myColor = spriteR.color;

		//game manager is executed (Script Exuction Order) first then this script
		switch (type) {
		case CollectableType.COIN:
			target = GameManager.GetInstance.CoinIcon;
			break;
		case CollectableType.FOOD:
			target = GameManager.GetInstance.FoodIcon;
			break;
		case CollectableType.LIFE_QUALITY:
			target = GameManager.GetInstance.LifeQualityIcon;
			break;
		case CollectableType.PEOPLE:
			target = GameManager.GetInstance.PeopleIcon;
			break;
		}
	}

	void OnEnable() {
		if (!spawnedNow) {
			animator.enabled = true;
			beingCollected = false;
			Reset ();
			rb.velocity = new Vector2 (Random.Range (-rangeForce, rangeForce), Random.Range (-rangeForce, rangeForce));
			if (type.Equals (CollectableType.COIN)) {
				StartCoroutine (BeCollected ());
			} else {
				BeCollectedNow ();
			}
		} else {
			spawnedNow = false;
		}
	}

	void Update () {
		if(type.Equals(CollectableType.COIN)) {
			CheckTouch();
		}
		if (beingCollected) {
			transform.localScale = Vector3.Lerp (transform.localScale, Vector3.one / 2f, Time.deltaTime);
			if (!particles.isPlaying && hasBlasted) {
				gameObject.SetActive (false);
			}
		}
	}

	void FixedUpdate () {
		if (beingCollected) {
			rb.MovePosition (Vector2.SmoothDamp (rb.position, target.position, ref velocity, smooth, distance * speed, Time.fixedDeltaTime));
		}
	}

	void OnTriggerEnter2D (Collider2D coll) {
		if (coll.CompareTag (type.ToString())){
			animator.enabled = false;
			transform.rotation = Quaternion.identity;
			spriteR.enabled = false;
			particles.Play ();
			InstantCollect (value, type);
			hasBlasted = true;
		}
	}

	#endregion //unity messages
	#region my methods

	public static void InstantCollect(int value, CollectableType collectType) {
		switch (collectType) {
		case CollectableType.COIN:
			GameManager.GetInstance.Money += value;
			break;
		case CollectableType.FOOD:
			GameManager.GetInstance.Food += value;
			break;
		case CollectableType.LIFE_QUALITY:
			GameManager.GetInstance.LifeQuality += value;
			break;
		case CollectableType.PEOPLE:
			GameManager.GetInstance.People += value;
			break;
		}
	}

	private void Reset() {
		distance = 0;
		velocity = Vector2.zero;
		hasBlasted = false;
		particles.Stop ();
		spriteR.enabled = true;
		if (type.Equals (CollectableType.COIN)) {
			ScaleAdjust ();
		}
	}

	private IEnumerator BeCollected() {
		yield return new WaitForSeconds (5f);
		BeCollectedNow ();
	}

	//check the touch to collect the coins early, here the coin soundfx is played
	private void CheckTouch() {
		if (Input.touchCount > 0) {
			for (int i = 0; i < Input.touchCount; i++) {
				if (Input.GetTouch (i).phase.Equals (TouchPhase.Began)) {
					Vector2 touchCoord = Input.GetTouch (i).position;
					Vector3 touchPosition = Camera.main.ScreenToWorldPoint (new Vector3 (touchCoord.x, touchCoord.y));
					RaycastHit2D hit = Physics2D.Raycast (touchPosition, Vector2.zero, distance);
					if (hit.collider != null && hit.collider.gameObject.Equals(gameObject)) {
						BeCollectedNow ();
						AudioManager.GetInstance.Play (SoundFX.COIN);
					}
				}//end of touch phase
			}//end of for
		}//end of if touch count
	}//end of check touch method

	//ignore the distance, and just collect it and cancel the c
	public void BeCollectedNow() {
		StopCoroutine ("BeCollected");
		beingCollected = true;
		distance = Vector2.Distance (rb.position, target.position);
	}

	private void ScaleAdjust() {
		switch (coinType) {
		case CoinType.COIN:
			transform.localScale = Vector3.one;
			break;
		case CoinType.SUPER_COIN:
			transform.localScale = Vector3.one * 1.25f;
			break;
		case CoinType.MEGA_COIN:
			transform.localScale = Vector3.one * 1.5f;
			break;
		}
	}//end scale adjust

	public int Value {
		get { return value; }
		set {
			this.value = value;
			spriteR.color = this.value > 0 ? myColor : Color.gray;
		}
	}

	#endregion //my methods
}
