﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public enum ServerStatus {
	WAITING,
	LOGGING_IN,
	LOGGING_OUT,
	GETTING_AUTHORIZATION,
	CONNECTED,
	ERROR
}

public class ServerConnector : MonoBehaviour {

	#region FIELDS
	//constants
	private const string URL = "http://dev.pushstart.com.br/desafio/public/api";
	private const string URL_AUTHORIZATION = URL + "/status";
	private const string URL_AUTHENTICATION = URL + "/auth/login";
	private const string URL_LOGOUT = URL + "/auth/logout";


	//singleton field
	private static ServerConnector serverConnector;

	//properties showed on inspector
	[SerializeField] private string gameplayScene;


	//other fields
	public string errorMessage { get; private set; }
	public ServerStatus status { get; private set; }
	public LoginInfo loginInfo { get; private set; }
	public MySceneManager sceneManager;
	public AudioManager audioManager;

	#endregion //fields

	#region Connection Area
	public void AuthenticationRequest (string profileInfo) {
		StartCoroutine (Authentication (profileInfo));
	}

	public void LogoutRequest () {
		audioManager.Stop ();
		//empty the login information letting the GC collect it and let the token expires for its own
		loginInfo = null;
		sceneManager.ChangeScene ("StartMenu");
	}

	//this methods make the request for authentication by encoding the profileInfo
	//sended by Authenticator instance that pass the argument with username and
	//encrypted password.
	private IEnumerator Authentication (string profileInfo) {
		status = ServerStatus.LOGGING_IN;
		UnityWebRequest unityRequest = new UnityWebRequest (URL_AUTHENTICATION, UnityWebRequest.kHttpVerbPOST);
		byte[] bytes = System.Text.Encoding.UTF8.GetBytes (profileInfo);

		//here, the request header for json transfer is implemented and
		//the upload (with the serialized json data) and download instances are setted
		unityRequest.SetRequestHeader ("Content-Type","application/json");
		unityRequest.uploadHandler = new UploadHandlerRaw (bytes) as UploadHandler;
		unityRequest.downloadHandler = new DownloadHandlerBuffer () as DownloadHandler;

		yield return unityRequest.SendWebRequest ();
		if (unityRequest.isNetworkError || unityRequest.isHttpError) {
			//error treating
			status = ServerStatus.ERROR;
			switch (unityRequest.responseCode) {
			case 0:
				status = ServerStatus.ERROR;
				errorMessage = "Connection Failed: was impossible to conect with server. Verify if you are connected to the internet.";
				break;
			case 401:
				errorMessage = "Incorrect username or password";
				break;
			case 503:
				errorMessage = "We are in maintenance, please come back later.";
				break;
			default:
				errorMessage = "Error " + unityRequest.responseCode + ": Unknowing error";
				break;
			}
		} else {
			//if everything happen normaly, the json are extracted from download instance
			//and the authorization to continue running the login flow
			status = ServerStatus.GETTING_AUTHORIZATION;
			loginInfo = JsonUtility.FromJson<LoginInfo>(unityRequest.downloadHandler.text);
			StartCoroutine (GetAuthorization ());
		}
			
	}//end of coroutine

	//when the user logged with success, the authorization to keep running is requested
	//here, the authorization is requested passing the token received on authentication
	private IEnumerator GetAuthorization() {
		using (UnityWebRequest unityRequest = UnityWebRequest.Get (URL_AUTHORIZATION)) {
			unityRequest.SetRequestHeader ("X-Authorization", loginInfo.token);
			yield return unityRequest.SendWebRequest ();
			//error treating
			if (unityRequest.isNetworkError || unityRequest.isHttpError) {
				status = ServerStatus.ERROR;
				switch (unityRequest.responseCode) {
				case 401:
				default:
					errorMessage = "Unknown authorized error: please, contact our support";
						break;
					
				}
			} else {
				status = ServerStatus.CONNECTED;
				//if everything work fine, the user profile is prepared to gameplay and the
				//scene is changed
				JsonUtility.FromJsonOverwrite (unityRequest.downloadHandler.text, loginInfo.profile);
				audioManager.Stop ();
				sceneManager.ChangeScene (gameplayScene);
			}
		}//end of using
	}//end of coroutine
		
	#endregion //Connection Area

	#region Singleton Area
	void Awake () {
		status = ServerStatus.WAITING;

		//assign singleton field
		DontDestroyOnLoad (gameObject);
		if (serverConnector != null) {
			Destroy (gameObject);
		} else {
			serverConnector = this;
		}
	}//awake method end

	public void UsernameOrPasswordAreEmpty() {
		errorMessage = "Username or password is missing, please fill it";
		status = ServerStatus.ERROR;
	}

	//singleton get;
	public static ServerConnector GetInstance {
		get { return serverConnector != null ? serverConnector : serverConnector = GameObject.FindObjectOfType<ServerConnector>(); }
	}
	#endregion //singleton area







}//end of class
