﻿using System.Security.Cryptography;
using System;
using System.Text;

public static class Security {

	public static string ConvertToSHA256(string str) {
		StringBuilder hashBuilder = new StringBuilder (String.Empty);
		SHA256Managed encryptor = new SHA256Managed ();

		//get the bytes of string parameter encoded using UTF8 format
		byte[] bytes = Encoding.UTF8.GetBytes (str);
		bytes = encryptor.ComputeHash (bytes); //encrypt momment

		//takes every array position and join on string builder variable
		//using only two places each tyme (two places on hexadecimal means 256 characters)
		for (int i = 0; i < bytes.Length; i++) {
			hashBuilder.Append (String.Format ("{0:x2}", bytes[i]));
		}
		return hashBuilder.ToString();
	}
}
